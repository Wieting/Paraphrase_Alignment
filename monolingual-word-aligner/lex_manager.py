class LexManager:
    def __init__(self):
        self.featstr2id={}
        self.id2featstr={}

    def getFeatString(self, id):
        return self.id2featstr[id]

    def getId(self, featstr):
        return self.featstr2id[featstr]

    def getTotalNumFeats(self):
        return len(self.id2featstr)

    def containsFeatstr(self, featstr):
        return featstr in self.featstr2id

    def addFeaturestr(self, featstr):
        if(featstr not in self.featstr2id):
            n = len(self.featstr2id)
            self.featstr2id[featstr]=n
            self.id2featstr[n]=featstr

    def getAllFeatstr(self):
        return self.featstr2id.keys()

    def getX(self,featmap):
        X= {}
        for i in featmap:
            n = featmap[i]
            idx = self.featstr2id[i]
            X[idx]=n
        return X

if __name__ == '__main__':
    lex = LexManager()
    lex.addFeaturestr('equal str')
    lex.addFeaturestr('similar str')
    print lex.getFeatString(1)
    print lex.getId('similar str')
    print lex.getTotalNumFeats()
    print lex.containsFeatstr('equal str')
    print lex.getAllFeatstr()

    fmap = {}
    fmap['equal str']=.1
    fmap['similar str']=.7
    X = lex.getX(fmap)
    print X
