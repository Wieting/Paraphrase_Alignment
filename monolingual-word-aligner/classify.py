from libsvm.python.svmutil import *

y, x = svm_read_problem('features-train.txt')
m = svm_train(y, x, '-t 0 -c 10')
p_label, p_acc, p_val = svm_predict(y, x, m)
#print p_label, p_acc, p_val

lines = open('features-train.txt','r')
lines = lines.readlines()

max = 0
for i in lines:
    i =i.split()
    v = i[1].split(':')[1]
    v = float(v)
    tot=0
    cor=0
    for j in lines:
        j =j.split()
        vv = j[1].split(':')[1]
        vv = float(vv)
        if(vv < v and j[0]=='-1'):
            cor=cor+1
        if (vv >= v and j[0]=='1'):
            cor=cor+1
        tot = tot + 1
    acc = float(cor)/tot
    if(acc > max):
        max = acc

print max