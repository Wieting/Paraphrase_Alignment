from aligner import *
from lex_manager import LexManager
import pickle

def writeFeatures(ys,xs,out):
    fout = open(out,'w')
    for i in range(len(ys)):
        y = ys[i]
        x = xs[i]
        ll= str(y) +'\t'
        for i in x:
            ll = ll + str(i+1) +':'+str(x[i])+'\t'
        fout.write(ll+'\n')


f = open('semeval2014/SICK_trial.txt','r')
lines = f.readlines()

als = pickle.load(open('semeval2014/SICK_trial.txt.pickle.save','r'))
pt1 = pickle.load(open('semeval2014/SICK_trial.txt.s1.pickle.save','r'))
pt2 = pickle.load(open('semeval2014/SICK_trial.txt.s2.pickle.save','r'))

ys = []
xs = []

lex = LexManager()
for i in range(len(lines)):
    if i==0:
        continue
        #if(i==5):
        #break
    ll = lines[i].split('\t')
    s1 = ll[1]
    s2 = ll[2]
    score = ll[3]
    score = float(score)

    alignments = als[i-1]
    sentence1ParseResult = pt1[i-1]
    sentence2ParseResult = pt2[i-1]
    #print alignments[0]
    #print alignments[1]

    #sentence1ParseResult = parseText(s1)
    #sentence2ParseResult = parseText(s2)

    sentence1Lemmatized = lemmatize(sentence1ParseResult)
    sentence2Lemmatized = lemmatize(sentence2ParseResult)

    #print sentence1Lemmatized
    #print sentence2Lemmatized

    n1 = len(sentence1Lemmatized)
    n2 = len(sentence2Lemmatized)

    #print len(alignments[0]),n1,n2, score
    y = 1
    if score < 4:
        y = -1
    lex.addFeaturestr('percent_align')
    fmap = {}
    tot= n1
    if n2 < tot:
        tot = n2
    f = len(alignments[0])
    fmap['percent_align']=f/float(tot)
    x = lex.getX(fmap)
    print y, x
    xs.append(x)
    ys.append(y)

writeFeatures(ys,xs,'features-train.txt')


#sentence1PosTagged = posTag(sentence1ParseResult)
#sentence2PosTagged = posTag(sentence2ParseResult)

#print sentence1PosTagged

