import sys
import pickle
from aligner import *

f = open(sys.argv[1],'r')
lines = f.readlines()

alignments=[]
sentence1=[]
sentence2=[]

for i in range(len(lines)):
    if i==0:
        continue
    #if(i==5):
    #    break
    ll = lines[i].split('\t')
    s1 = ll[1]
    s2 = ll[2]
    score = ll[3]
    score = float(score)
    al = align(s1,s2)
    alignments.append(al)
    sentence1ParseResult = parseText(s1)
    sentence2ParseResult = parseText(s2)
    sentence1.append(sentence1ParseResult)
    sentence2.append(sentence2ParseResult)

pfile = open(sys.argv[1]+'.pickle.save','w')
pickle.dump(alignments,pfile)
pfile = open(sys.argv[1]+'.s1.pickle.save','w')
pickle.dump(sentence1,pfile)
pfile = open(sys.argv[1]+'.s2.pickle.save','w')
pickle.dump(sentence2,pfile)
